package com.hfkh.java54.liupan.net.FTP;

import java.io.File;
import java.util.Scanner;

public class Server {
	static Scanner input = new Scanner(System.in);
	public static void main(String[] args) {
		showRootMenu();
	}

	private static void showRootMenu() {
		System.out.println("**********欢迎使用本地文件系统**********");
		//显示所有盘符
		File [] disks = File.listRoots();//列举盘符
		for(int i = 0;i < disks.length;i++){
			System.out.print((i+1)+"."+disks[i]+"\t");
		}

		System.out.println("\n请选择：");
		int num = input.nextInt();
		lookDirectory(disks[num-1]);
	}
	/**
	 * 递归方法，查看目录及返回
	 * @param file
	 */
	private static void lookDirectory(File file) {
		File [] fs = file.listFiles();//列举文件
		for (int i = 0; i < fs.length; i++) {
			System.out.println((i+1)+"."+fs[i]);
		}

		System.out.println("\n请选择(按0返回上一级；按-1返回根目录)：");
		int num = input.nextInt();
		if(num==0){
			if(file.getParentFile()==null){
				showRootMenu();
			}else{
				lookDirectory(file.getParentFile());
			}
			return;//防止迭代
		}else if(num==-1){
			showRootMenu();
			return;//防止迭代
		}

		if(fs[num-1].isDirectory()){
			lookDirectory(fs[num-1]);
		}else{
			showFileMenu(fs[num-1]);
			lookDirectory(file.getParentFile());
		}
	}

	private static void showFileMenu(File file) {
		System.out.println("1.删除2.修改3.重命名4.新建");
	}





}
