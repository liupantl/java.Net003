package liupan001;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class RandomTest {
	public static void main(String[] args) {
		List<Integer> numList =new ArrayList<Integer>();
		//打印50个0-300的整数随机数
		int num = 0;
		System.out.println("0-300的50个整数随机数如下：");
		for(int i=1;i<=50;i++){
			num =(int)( Math.random()*300);
			numList.add(num);
			System.out.print(num+" ");
		}

		System.out.println("\n偶数是：");
		//找到50个随机数中的偶数，并存入集合中
		List<Integer>num2List = new ArrayList<Integer>();	
		for (int i = 0; i <numList.size(); i++) {
			if(numList.get(i)%2==0){
				num2List.add(numList.get(i));
			}
		}
		//将集合转换成数组
		int [] nums = new int[num2List.size()];
		for (int i = 0; i < num2List.size(); i++) {
			nums[i]=num2List.get(i);
		}
		//将数组按升序排列
		Arrays.sort(nums);
		//将数组按降序排列
		for (int i =nums.length-1; i >0; i--) {
			System.out.print(nums[i]+" ");
		}
	}
}
