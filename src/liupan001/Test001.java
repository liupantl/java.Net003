package liupan001;
/**
 * 假设有50瓶饮料，喝完3个空瓶可以换一瓶饮料，依次类推，请问一共喝了多少瓶饮料？
 * @author Administrator
 *
 */
public class Test001 {
	public static void main(String[] args) {
		int count =0;
		for (int i =50; i>0; i--) {
			count++;
			//每喝完3瓶，i+1
			if(count%3==0){
				i++;
			}
		}
		System.out.println("一共喝了"+count+"瓶饮料");
	}
}
