package liupan001;

import java.util.Arrays;
import java.util.Scanner;

public class HangZhouMianShiTi {
	public static void main(String[] args) {
		String words = "ABCDEFGHIJKLMNOPQRSTUVWXYZ*";
		//分割成3段
		String[] strs = new String[3];
		int j = 0;
		for(int i = 0;i < 19;i += 9){
			strs[j] = words.substring(i, i+9);
			j++;
		}
		//遍历数组
		for(String ss:strs){
			System.out.println(ss);
		}

		Scanner input = new Scanner(System.in);
		System.out.println("请输入月份：");
		int month = input.nextInt();
		System.out.println(Arrays.toString(loopLeft(strs, month-1)));
		System.out.println("请输入日期：");
		int day = input.nextInt();

		System.out.println("请输入信息：");
		String msg = input.next();
		char [] msgs = msg.toCharArray();
		System.out.println(Arrays.toString(msgs));

		char[] msgs1 = strs[0].toCharArray();
		char[] msgs2 = strs[1].toCharArray();
		char[] msgs3 = strs[2].toCharArray();
		char[][]a = {msgs1,msgs2,msgs3};

		for(int i = 0;i < strs.length;i++){
			System.out.println(Arrays.toString(loopLeft(a[i],day-1)));
		}

		for (int i = 0; i < msgs.length; i++) {
			for (int k = 0; k < a.length; k++) {
				for (int k2 = 0; k2 < a[k].length; k2++) {
					if(msgs[i]==a[k][k2]){
						System.out.print((k+1)+""+(k2+1)+"  ");
					}
				}
			}
		}
	}

	public static String [] loopLeft(String [] ss,int count){
		// 把最后一个往前移动
		// a b c  0 1 2
		// b b c
		// b c a
		for(int j = 0;j<count;j++){
			String temp = ss[0];
			for(int i = 0;i<ss.length-1;i++){		
				ss[i] = ss[i+1];
			}
			ss[ss.length-1] = temp;
		}
		return ss;
	}

	public static char [] loopLeft(char [] ss,int count){
		// 把最后一个往前移动
		// a b c  0 1 2
		// b b c
		// b c a
		for(int j = 0;j<count;j++){
			char temp = ss[0];
			for(int i = 0;i<ss.length-1;i++){		
				ss[i] = ss[i+1];
			}
			ss[ss.length-1] = temp;
		}
		return ss;
	}

	//abcdefghi
	//bcdefghia
	//cdefghiab
	//defghiabc
	//efghiabcd
	//fghiabcde
	//ghiabcdef
	//hiabcdefg
	//	System.out.println(Arrays.toString(loopLeft
	//			(new String[]{"A","B","C","D","E","F","G","H","I"}, 7)));

}
