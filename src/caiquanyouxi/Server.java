package com.java.liupan.caiquanyouxi;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

public class Server {
	public static void main(String[] args) {
		new Server();
	}
	//服务器
	ServerSocket server;
	//代表所有的客户端线程
	static List<ClientThread> cts = new ArrayList<ClientThread>();

	public Server(){
		try {
			server = new ServerSocket(9999);
			System.out.println("服务器已经启动！");
			while(true){
				//不断接收新客户端的请求
				Socket client = server.accept();
				ClientThread ct = new ClientThread(client);
				cts.add(ct);
				ct.start();
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}

//用于处理客户端请求的线程
class ClientThread extends Thread{
	private Socket socket;
	//用于向当前socket客户端写
	ObjectOutputStream oos ;
	//用于读取当前socket客户端的内容
	ObjectInputStream ois ;
	//当前线程出拳数
	int num;
	public ClientThread(Socket socket) {
		super();
		this.socket = socket;
	}

	@Override
	public void run() {
		try {
			//用于向当前socket客户端写
			oos= new ObjectOutputStream(socket.getOutputStream());
			//用于读取当前socket客户端的内容
			ois = new ObjectInputStream(socket.getInputStream());
			String name = ois.readUTF();
			System.out.println(name+"加入了比赛");
			//向所有客户端发送游戏人数
			for(ClientThread ct : Server.cts){
				ct.oos.writeInt(Server.cts.size());
				ct.oos.flush();
			}

			num = ois.readInt();
			ClientThread c1 = Server.cts.get(0);
			ClientThread c2 = Server.cts.get(1);

			oos.writeBoolean(c1.num!=0 && c2.num!=0);
			oos.flush();


			if(c1.num!=0 && c2.num!=0){
				//1.石头2.剪刀3.布
				if(c1.num==c2.num){
					c1.oos.writeUTF("平局");
					c2.oos.writeUTF("平局");
				}else if(c1.num==1&&c2.num==2 || c1.num==2&&c2.num==3 || c1.num==3&&c2.num==1){
					c1.oos.writeUTF("恭喜，您赢了！");
					c2.oos.writeUTF("很可惜，您输了！");
				}else{
					c2.oos.writeUTF("恭喜，您赢了！");
					c1.oos.writeUTF("很可惜，您输了！");
				}
			}
			c1.oos.flush();
			c2.oos.flush();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
	}
}