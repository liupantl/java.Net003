package com.java.liupan.caiquanyouxi.copy;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.Scanner;

public class Client {
	public static void main(String[] args) {
		try {
			Socket client = new Socket("localhost",9999);//连接服务器
			ObjectOutputStream oos= new ObjectOutputStream(client.getOutputStream());
			ObjectInputStream ois = new ObjectInputStream(client.getInputStream());

			Scanner input = new Scanner(System.in);
			System.out.println("请输入您的昵称：");
			String name = input.next();
			oos.writeUTF(name);
			oos.flush();

			int clientCount = ois.readInt();
			if(clientCount==1){
				System.out.println("还差一人，请稍等......");
				clientCount = ois.readInt();
				System.out.println("人数已够，请"+name+"开始游戏！");
			}else if(clientCount==2){
				System.out.println("请开始游戏！");
			}
			
			System.out.println("请出拳：1.石头2.剪刀3.布");
			int num = input.nextInt();
			oos.writeInt(num);
			oos.flush();
			//判断对方是否出拳，如果没有出拳，则提示当前线程客户端，对方还未出拳
			if(!ois.readBoolean()){
				System.out.println("对方还没出拳，请稍后......");
			}
			
			System.out.println(ois.readUTF());

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
