package com.hfkh.java54.liupan.netqianghongbao;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

public class Server {
	public static void main(String[] args) {
		new Server();
	}
	ServerSocket server;//服务器
	//客户端线程
	static List <ClientThread> socketList = new ArrayList <ClientThread>();
	public Server() {
		try {
			server = new ServerSocket(9999);
			System.out.println("服务器已经启动！");
			while(true){
				Socket client = server.accept(); 
				ClientThread ct = new ClientThread(client);
				socketList.add(ct);
				ct.start();
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}

class ClientThread extends Thread{
	private Socket socket;
	//向该客户端写
	ObjectOutputStream oos;
	//读取该客户端的内容
	ObjectInputStream ois;
	int num;//选择发红包还是抢红包
	String name;//姓名
	int pNum;//人数
	static int count;
	static ClientThread faHongBaoCT;//发红包客户端
	static List <String> hongBaoList;//抢红包客户端
	public ClientThread(Socket socket) {
		super();
		this.socket = socket;
	}
	@Override
	public void run() {
		try {
			oos = new ObjectOutputStream(socket.getOutputStream());
			ois = new ObjectInputStream(socket.getInputStream());
			name =ois.readUTF();
			System.out.println(name+"已经上线！");

			num = ois.readInt();

			if(num==1 ){
				faHongBaoCT = this;//当前客户端发红包
				double money = ois.readDouble();//发多少钱
				pNum = ois.readInt();           //发给多少人
				hongBaoList=HongBaoUtil.qiangHongBao(money);//调用红包算法并赋值，得到红包集合

			} else {
				if(count==faHongBaoCT.pNum){
					oos.writeUTF(faHongBaoCT.name+"的红包已经被抢完了！");
				}else{
					String moneyStr = hongBaoList.get(count++);
					oos.writeUTF("你抢了"+faHongBaoCT.name+"红包："+moneyStr);
					faHongBaoCT.oos.writeUTF(name+"抢了你的红包，"+moneyStr);
					faHongBaoCT.oos.flush();
				}
				oos.flush();
			}
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
	}
}