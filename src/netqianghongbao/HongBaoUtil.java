package com.hfkh.java54.liupan.netqianghongbao;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class HongBaoUtil {
	public static List<String>  qiangHongBao(double m){
		List <String> hongBaoList = new ArrayList <String> ();
		int money = (int) (m*100);
		Random r = new Random();
		while(true){
			boolean flag = true;
			for (int i = 1; i <= 10; i++) {
				int randMoney = r.nextInt(money);
				String randMoneyStr =randMoney+"";
				
				if(randMoneyStr.length()==3){
					hongBaoList.add(randMoneyStr.substring(0, 1)+"."+randMoneyStr);
				}else if(randMoneyStr.length()==2){
					hongBaoList.add("0."+randMoneyStr);
				}else if(randMoneyStr.length()==1){
					hongBaoList.add("0.0"+randMoneyStr);
				}
				
				money -= randMoney;
				if(randMoney==0){
					flag = false;
					break;
				}
			}
			money = (int) (m*100);
			if(flag){
				break;
			}else{
				hongBaoList.clear();
			}
		}
		return hongBaoList;
	}
	
	public static void main(String[] args) {
		System.out.println(HongBaoUtil.qiangHongBao(10));
	}
}
