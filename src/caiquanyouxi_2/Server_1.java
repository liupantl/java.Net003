package com.java.liupan.caiquanyouxi_2;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

public class Server_1 {
	public static void main(String[] args) {
		new Server_1();
	}

	ServerSocket server;
	static List <ClientThread> socketList = new ArrayList <ClientThread> ();
	public Server_1() {
		try {
			server = new ServerSocket(9999);
			System.out.println("服务器已开启！");
			while(true){
				Socket client = server.accept();
				ClientThread ct = new ClientThread(client);
				socketList.add(ct);
				ct.start();
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}

class ClientThread extends Thread{
	private Socket socket;
	ObjectOutputStream oos;
	ObjectInputStream ois;
	int num;


	public ClientThread(Socket socket) {
		super();
		this.socket = socket;
	}
	@Override
	public void run() {
		try {
			oos = new ObjectOutputStream(socket.getOutputStream());
			ois = new ObjectInputStream(socket.getInputStream());
			String name = ois.readUTF();
			System.out.println(name+"已经上线！");
			//向所有客户端发送游戏人数
			for(ClientThread ct:Server_1.socketList){
				ct.oos.writeInt(Server_1.socketList.size());
				ct.oos.flush();
			}

			num = ois.readInt();
			ClientThread c1 = Server_1.socketList.get(0);
			ClientThread c2 = Server_1.socketList.get(1);
			//判断是否两个人都已经出拳，将结果写向客户端
			oos.writeBoolean(c1.num!=0 && c2.num!=0);
			oos.flush();
			if(c1.num!=0 && c2.num!=0){
				//1.石头2.剪刀3.布
				if(c1.num==c2.num){
					c1.oos.writeUTF("平局");
					c2.oos.writeUTF("平局");
				}else if((c1.num==1 && c2.num==2) || (c1.num==2 && c2.num==3) || (c1.num==3 && c2.num==1)){
					c1.oos.writeUTF("恭喜你赢了！");
					c2.oos.writeUTF("不要灰心，再接再厉哦！");
				}else{
					c1.oos.writeUTF("不要灰心，再接再厉哦！");
					c2.oos.writeUTF("恭喜你赢了！");
				}
			}
			c1.oos.flush();
			c2.oos.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
	}
}