package com.java.liupan.caiquanyouxi_1;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.Scanner;

public class Client {
	public static void main(String[] args) {
		try {
			Socket client = new Socket("localhost",9999);
			ObjectOutputStream oos = new ObjectOutputStream(client.getOutputStream());
			ObjectInputStream ois = new ObjectInputStream(client.getInputStream());
			Scanner input = new Scanner(System.in);
			System.out.println("请输入用户名：");
			String name = input.next();
			oos.writeUTF(name);
			oos.flush();

			int clientCount = ois.readInt();
			if(clientCount==1){
				System.out.println("对上还未上线，请稍后......");
				clientCount = ois.readInt();
				System.out.println("游戏开始");
			}else{
				System.out.println("游戏开始");
			}
			System.out.println("请出拳：1.石头2.剪刀3.布");
			int num = input.nextInt();
			oos.writeInt(num);
			oos.flush();

			if(!ois.readBoolean()){
				System.out.println("对方还未出拳，请稍等......");
			}
			System.out.println(ois.readUTF());	
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
	}
}
