package com.hfkh.java54.liupan.netqianghongbao_1;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.Scanner;

public class Client {
	public static void main(String[] args) {
		try {
			Socket client = new Socket("localhost",9999);
			ObjectOutputStream oos = new ObjectOutputStream(client.getOutputStream());
			ObjectInputStream ois = new ObjectInputStream(client.getInputStream());
			
			Scanner input = new Scanner(System.in);
			System.out.println("请输入用户名：");
			String name = input.next();
			oos.writeUTF(name);
			oos.flush();
			
			System.out.println("请选择：1.发红包2.抢红包");
			int num = input.nextInt();
			oos.writeInt(num);
			oos.flush();
			if(num==1){
				System.out.println("请输入红包金额：");
				double money = input.nextDouble();
				oos.writeDouble(money);
				
				System.out.println("请输入红包数量：");
				int pNum = input.nextInt();
				oos.writeInt(pNum);
				oos.flush();
				for (int i = 0; i < pNum; i++) {
					System.out.println(ois.readUTF());
				}
			}else{
				System.out.println(ois.readUTF());
			}
			System.out.println(ois.readUTF());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
