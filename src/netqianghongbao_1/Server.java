package com.hfkh.java54.liupan.netqianghongbao_1;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

public class Server {
	public static void main(String[] args) {
		new Server();
	}
	ServerSocket server;
	static List <ClientThread> clientList = new ArrayList <ClientThread>();
	public Server() {
		try {
			server = new ServerSocket(9999);
			System.out.println("服务器已经开启！");
			while(true){
				Socket client = server.accept();
				ClientThread ct = new ClientThread(client);
				clientList.add(ct);
				ct.start();
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}


class ClientThread extends Thread{
	private Socket socket;
	String name;
	int num;
	int pNum;
	static int count;
	ObjectOutputStream oos;
	ObjectInputStream ois;
	static ClientThread faHongBaoCT;
	static List<String> hongBaoList;
	public ClientThread(Socket socket) {
		super();
		this.socket = socket;
	}
	@Override
	public void run() {
		try {
			oos = new ObjectOutputStream(socket.getOutputStream());
			ois = new ObjectInputStream(socket.getInputStream());
			name = ois.readUTF();
			System.out.println(name+"已经上线！");
			
			num = ois.readInt();
			if(num==1){
				faHongBaoCT = this;
				double money = ois.readDouble();
				pNum = ois.readInt();
				hongBaoList = HongBaoUtil.qiangHongBao(money);
			}else{
				if(count==faHongBaoCT.pNum){
					oos.writeUTF(faHongBaoCT.name+"的红包已经被抢完了！");
				}else{
					String moneyStr = hongBaoList.get(count++);
					oos.writeUTF("你抢了"+faHongBaoCT.name+"的红包："+moneyStr+"元！");
					faHongBaoCT.oos.writeUTF("你的红包被抢了"+moneyStr+"元");
					faHongBaoCT.oos.flush();
				}
				oos.flush();
			}
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}